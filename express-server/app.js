var express = require("express"), cors = require('cors');
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var noticias = [
    "Literatura París",
    "Futbol Barcelona",
    "Futbol Barranquilla",
    "Política Montevideo",
    "Economía Santiago de Chile",
    "Cocina CDMX",
    "Finanzas Nueva York"
];
app.get(
    "/get", 
    (req, res, next) => 
        res.json(noticias.filter((c) => 
            c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)
        )
);
var news = [
    { "id":1,
      "titulo": "Literatura París",
      "categoria": "Cultura",
      "nota": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "countReview": 1,
      "leyendo": false,
      "favorita": false 
    }, 
    { "id":2,
      "titulo": "Futbol Barcelona",
      "categoria": "Deportes",
      "nota": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "countReview": 1,
      "leyendo": false,
      "favorita": false 
    },
    { "id":3,
      "titulo": "Futbol Barranquilla",
      "categoria": "Deportes",
      "nota": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "countReview": 1,
      "leyendo": false,
      "favorita": false 
    },
    { "id":4,
      "titulo": "Política Montevideo",
      "categoria": "Política",
      "nota": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "countReview": 1,
      "leyendo": false,
      "favorita": false 
    },
    { "id":5,
      "titulo": "Economía Santiago de Chile",
      "categoria": "Economía y Finanzas",
      "nota": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "countReview": 1,
      "leyendo": false,
      "favorita": false 
    },
    { "id":6,
      "titulo": "Cocina CDMX",
      "categoria": "Gastronomia",
      "nota": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "countReview": 1,
      "leyendo": false,
      "favorita": false 
    },
    { "id":7,
      "titulo": "Finanzas Nueva York",
      "categoria": "Economía y Finanzas",
      "nota": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
      "countReview": 1,
      "leyendo": false,
      "favorita": false 
    }    
  ];
/*la url seria http://localhost:3000/get?q=*/
app.get(
    "/news", 
    (req, res, next) => 
        res.json(
            news.filter(
                (c) => c.titulo.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1
            )
        )
);
app.post(
    "/news", 
    (req, res, next) => {
        console.log(req.body);
        news.push(req.body.nuevo);
        res.json(news);
    }
);

var misFavoritos = [];
app.get("/favs", (req, res, next) => res.json(misFavoritos));
app.post("/favs", (req, res, next) => {
    console.log(req.body);
    misFavoritos.push(req.body.nuevo);
    res.json(misFavoritos);
});
app.put("/favs", (req, res, next) => {
    console.log(req.body);    
    const index = misFavoritos.findIndex(x => x.id === req.body.update.id)
    misFavoritos[index] = req.body.update;
    console.log(misFavoritos);
    res.json(misFavoritos);
});